from datetime import date

import pytest
from arrow.parser import ParserError

from elastic_indices import provider
from elastic_indices.enum import ElasticIndexSpanEnum as Span

test_definition = provider.ElasticIndexDefinition('test', Span.DAY)


def test_empty_string():
    try:
        test_definition.get('')
    except Exception as ex:
        if not isinstance(ex, ParserError):
            pytest.fail(ex)


def assert_range(definition):
    # type: (provider.ElasticIndexDefinition, int, int) -> None
    # noinspection PyProtectedMember
    prefix = definition._index_prefix

    def format_index(month):
        return '{}2017-{:02d}*'.format(prefix, month)

    expected = {format_index(tu) for tu in range(1, 12 + 1)}
    result = set(definition.get(date(year=2017, month=1, day=1), date(year=2017, month=12, day=1)))
    assert len(expected) == len(result)
    assert expected == result


def test_range_day():
    assert {'test2018-01*'} == set(test_definition.get('2018-01-01', '2018-01-31'))


def test_range_month():
    assert_range(test_definition)
