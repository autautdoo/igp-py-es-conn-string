import re


class InvalidESConnString(Exception):
    pass


class ESConnString:
    def __init__(self, conn_string):
        es_components_matcher = re.compile(r"^elastics?://(?:([^:]+):([^@]+)@)?(.*)$")
        try:
            username, password, nodes_str = es_components_matcher.match(conn_string).groups()
            using_ssl = conn_string.startswith("elastics://")
        except AttributeError:
            raise InvalidESConnString()

        host_matcher = re.compile(r"^([^:]+):?(\d+)?$")
        self._hosts = []
        for n in nodes_str.split(","):
            try:
                host, port = host_matcher.match(n).groups()
            except AttributeError:
                raise InvalidESConnString()
            port = port or "9200"
            protocol = "https" if using_ssl else "http"
            if username and password:
                self._hosts.append(f"{protocol}://{username}:{password}@{host}:{port}")
            else:
                self._hosts.append(f"{protocol}://{host}:{port}")

    @property
    def hosts(self):
        return self._hosts
